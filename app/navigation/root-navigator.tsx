import {
  createStackNavigator,
  NavigationContainer,
  createSwitchNavigator,
  createBottomTabNavigator,
  createAppContainer,
} from "react-navigation"
import { PostDetailScreen } from "../screens/postDetail-screen/postDetail-screen"
import { LoginScreen } from "../screens/login-screen/login-screen"
import { PostsScreen } from "../screens/posts-screen/posts-screen"
import SettingsScreen from "@app/screens/settings-screen/settings-screen"
import { BottomTabBarScreen } from "@app/screens/BottomTabBar-screen"
import { ProfileScreen, UserFollowers } from "@app/screens/profile-screen"
import { UserQuestions } from "@app/screens/profile-screen/subScreens/userQuestions"
export const RootNavigator: NavigationContainer = createStackNavigator(
  {
    mainScreen: { screen: PostsScreen },
    postDetailScreen: { screen: PostDetailScreen },
    ProfileScreen: { screen: ProfileScreen },
    _userFollowers: { screen: UserFollowers },
    _userQuestions: { screen: UserQuestions },
  },
  {
    headerMode: "screen",
    navigationOptions: { gesturesEnabled: true },
    initialRouteName: "mainScreen",
  },
)

export const QuestionsNavigator: NavigationContainer = createStackNavigator(
  { questionsScreen: { screen: PostsScreen }, postDetailScreen: { screen: PostDetailScreen } },
  { headerMode: "screen" },
)

export const PostsNavigator: NavigationContainer = createStackNavigator(
  { postsScreen: { screen: PostsScreen }, postDetailScreen: { screen: PostDetailScreen } },
  { headerMode: "screen" },
)

export const Settings: NavigationContainer = createStackNavigator(
  {
    SettingsScreen: { screen: SettingsScreen },
  },
  {
    headerMode: "screen",
  },
)

const AuthNavigator: NavigationContainer = createStackNavigator(
  {
    loginScreen: { screen: LoginScreen },
  },
  {
    headerMode: "screen",
    navigationOptions: { gesturesEnabled: true },
    initialRouteName: "loginScreen",
  },
)

const mainNavigator = createBottomTabNavigator(
  {
    RootNavigator,
    QuestionsNavigator,
    PostsNavigator,
    Settings,
  },
  {
    tabBarComponent: BottomTabBarScreen,
  },
)

const AppNavigator = createSwitchNavigator(
  {
    Auth: AuthNavigator,
    Home: mainNavigator,
  },
  {
    initialRouteName: "Auth",
  },
)
export default createAppContainer(AppNavigator)
