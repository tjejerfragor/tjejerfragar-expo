import * as SecureStore from "expo-secure-store"
/**
 * Saves some credentials securely.
 *
 * @param key The key
 * @param value The value
 */
export async function save(key: string, value: string) {
  return await SecureStore.setItemAsync(key, value)
}

/**
 * Loads credentials that were already saved.
 *
 * @param key The key that these creds are for
 */

export async function load(key: string) {
  return await SecureStore.getItemAsync(key)
}

/**
 * Resets any existing credentials for the given server.
 *
 * @param key The key which will be removed from keychain
 */
export async function reset(key: string) {
  return await SecureStore.deleteItemAsync(key)
}
