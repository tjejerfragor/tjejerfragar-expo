import { Linking } from "react-native"
import * as WebBrowser from "expo-web-browser"

interface HelperProps {
  isMale: (gender: "man" | "woman") => boolean
  openUrl: (url: string) => void
}

const openUrl = async (url: string) => {
  await WebBrowser.openBrowserAsync(url, { enableBarCollapsing: true })
}

export const helpers: HelperProps = {
  isMale: gender => (gender === "man" ? true : false),
  openUrl: (url: string) => openUrl(url),
}
