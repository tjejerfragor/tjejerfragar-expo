// Welcome to the main entry point of the app.
//
// In this file, we'll be kicking off our app or storybook.

import "./i18n"
import * as React from "react"
import { AppRegistry, View, Platform, StatusBar } from "react-native"
import { StorybookUIRoot } from "../storybook"
import { RootStore, setupRootStore } from "./models/root-store"
import { Provider, observer } from "mobx-react"
import { contains } from "ramda"
import { DEFAULT_NAVIGATION_CONFIG } from "./navigation/navigation-config"
import { AuthModel, Auth } from "./models/auth"
import { ApplicationProvider, Layout } from "react-native-ui-kitten"
import { mapping, light, dark } from "@eva-design/eva"
import { MainModel, Main } from "./models/main"
import { loadString } from "./utils/storage"
import { DynamicStatusBar } from "./components/dynamicStatusBar"
import moment from "moment"
import RootNavigator from "./navigation/root-navigator"
import { ApplicationLoader } from "./appLoader"
import { FollowModel, Follow } from "./models/follow"

export interface AppState {
  rootStore?: RootStore
  authStore?: Auth
  mainStore?: Main
  followStore?: Follow
}
require("moment/locale/sv")
moment.locale("sv")

let model
loadString("appTheme").then(resp => {
  if (!resp) {
    model = MainModel.theme = "light"
  }

  model = MainModel.setTheme(resp)
})

const fonts: { [key: string]: number } = {
  "opensans-bold": require("./assets/fonts/opensans-bold.ttf"),
  "opensans-light": require("./assets/fonts/opensans-light.ttf"),
  "opensans-regular": require("./assets/fonts/opensans-regular.ttf"),
  "opensans-semibold": require("./assets/fonts/opensans-semibold.ttf"),
  "opensans-italic": require("./assets/fonts/opensans-italic.ttf"),
}

const assets = {
  fonts: fonts,
  images: [],
}

/**
 * This is the root component of our app.
 */
@observer
export class App extends React.Component<{}, AppState> {
  /**
   * When the component is mounted. This happens asynchronously and simply
   * re-renders when we're good to go.
   */

  async componentDidMount() {
    this.setState({
      rootStore: await setupRootStore(),
    })
  }

  /**
   * Are we allowed to exit the app?  This is called when the back button
   * is pressed on android.
   *
   * @param routeName The currently active route name.
   */
  canExit(routeName: string) {
    return contains(routeName, DEFAULT_NAVIGATION_CONFIG.exitRoutes)
  }

  statusBarPadding = Platform.select({
    ios: 0,
    android: StatusBar.currentHeight,
  })

  render() {
    const rootStore = this.state && this.state.rootStore

    // Before we show the app, we have to wait for our state to be ready.
    // In the meantime, don't render anything. This will be the background
    // color set in native by rootView's background color.
    //
    // This step should be completely covered over by the splash screen though.
    //
    // You're welcome to swap in your own component to render if your boot up
    // sequence is too slow though.
    if (!rootStore) {
      return null
    }

    // otherwise, we're ready to render the app

    // --- am: begin list of stores ---
    const otherStores = {
      authStore: AuthModel,
      mainStore: MainModel,
      followStore: FollowModel,
    }

    // --- am: end list of stores ---

    let theme = MainModel.getCurrentTheme === "light" ? light : dark

    return (
      <ApplicationLoader assets={assets}>
        <Provider
          rootStore={rootStore}
          navigationStore={rootStore.navigationStore}
          {...otherStores}
        >
          <ApplicationProvider mapping={mapping} theme={theme}>
            <DynamicStatusBar currentTheme={MainModel.getCurrentTheme} />
            <Layout style={{ flex: 1, marginTop: this.statusBarPadding }}>
              <RootNavigator />
            </Layout>
          </ApplicationProvider>
        </Provider>
      </ApplicationLoader>
    )
  }
}

/**
 * This needs to match what's found in your app_delegate.m and MainActivity.java.
 */
const APP_NAME = "testbea"

// Should we show storybook instead of our app?
//
// ⚠️ Leave this as `false` when checking into git.
const SHOW_STORYBOOK = false

const RootComponent = SHOW_STORYBOOK && __DEV__ ? StorybookUIRoot : App

AppRegistry.registerComponent(APP_NAME, () => RootComponent)
