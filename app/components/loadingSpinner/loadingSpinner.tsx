import * as React from "react"
import { Props } from "./"
import { Text, Layout } from "react-native-ui-kitten"
import { ActivityIndicator, StyleSheet, View } from "react-native"

export function LoadingSpinner(props: Props) {
  const { isLoading } = props
  //TODO: gonna be loading spinner here when react-native-ui-kitten upgrades to v4.2.
  return (
    <Layout style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" animating={isLoading} />
    </Layout>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
})
