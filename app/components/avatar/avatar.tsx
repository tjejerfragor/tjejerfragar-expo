import * as React from "react"
import { Props, styles } from "./"
import { Touchable } from "../Touchable"
import { Avatar as EvaAvatar, AvatarProps } from "react-native-ui-kitten"
import { View } from "react-native"

export function Avatar(props: Props) {
  // grab the props
  const { userImageUrl, style, onPress, avatarSize, ...rest } = props

  return (
    <View style={style}>
      <Touchable onPress={onPress}>
        <EvaAvatar source={{ uri: userImageUrl }} size={avatarSize || "medium"} />
      </Touchable>
    </View>
  )
}
