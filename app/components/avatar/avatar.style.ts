import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  userImage: {
    width: 30,
    height: 30,
    borderRadius: 30,
    overflow: "hidden",
  },
})
