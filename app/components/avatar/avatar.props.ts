import { StyleType } from "react-native-ui-kitten"

export interface Props {
  userImageUrl: string
  onPress?: () => void
  avatarSize?: "tiny" | "small" | "medium" | "large" | "giant"
  style?: StyleType
}
