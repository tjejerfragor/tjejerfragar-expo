import { ViewStyle, TextStyle } from "react-native"

export interface Props {
  followers: number
  following: number
  posts: number
  onFollowersPress: () => void
  onFollowingPress: () => void
  onPostsPress: () => void
  style?: ViewStyle
  derivedTextStyle?: TextStyle
}
