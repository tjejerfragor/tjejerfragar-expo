import { StyleSheet } from "react-native"
import { fonts } from "@app/theme"

export const Style = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  parameterContainer: {
    alignItems: "center",
  },
  valueLabel: fonts.family.semibold,
  hintLabel: fonts.family.semibold,
})
