import * as React from "react"
import { Props, Style } from "./"
import { TouchableOpacity, View } from "react-native"
import { Text } from "react-native-ui-kitten/ui"
export function ProfileSocialDetails(props: Props) {
  const {
    derivedTextStyle,
    style,
    followers,
    following,
    posts,
    onFollowersPress,
    onFollowingPress,
    onPostsPress,
    ...restProps
  } = props

  return (
    <View {...restProps} style={[Style.container, style]}>
      <TouchableOpacity
        activeOpacity={0.65}
        style={Style.parameterContainer}
        onPress={onFollowersPress}
      >
        <Text style={[Style.valueLabel, derivedTextStyle]}>{`${followers}`}</Text>
        <Text style={[Style.hintLabel, derivedTextStyle]} appearance="hint" category="s2">
          Följare
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.65}
        style={Style.parameterContainer}
        onPress={onFollowingPress}
      >
        <Text style={[Style.valueLabel, derivedTextStyle]}>{`${following}`}</Text>
        <Text style={[Style.hintLabel, derivedTextStyle]} appearance="hint" category="s2">
          Följer
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.65}
        style={Style.parameterContainer}
        onPress={onPostsPress}
      >
        <Text style={[Style.valueLabel, derivedTextStyle]}>{`${posts}`}</Text>
        <Text style={[Style.hintLabel, derivedTextStyle]} appearance="hint" category="s2">
          Frågor
        </Text>
      </TouchableOpacity>
    </View>
  )
}
