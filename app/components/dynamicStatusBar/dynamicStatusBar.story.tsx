import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { DynamicStatusBar } from "./"

storiesOf("DynamicStatusBar", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <DynamicStatusBar text="DynamicStatusBar" />
      </UseCase>
    </Story>
  ))
