import * as React from "react"
import { Platform, TouchableHighlight, TouchableNativeFeedback } from "react-native"
import { Text } from "../text"
import TouchableProps from "./Touchable.props"

export function Touchable(props: TouchableProps) {
  const { onPress, children } = props

  return Platform.select({
    android: <TouchableNativeFeedback onPress={onPress}>{children}</TouchableNativeFeedback>,

    ios: <TouchableHighlight onPress={onPress}>{children}</TouchableHighlight>,
  })
}
