import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { Touchable } from "./"
import { Text } from "react-native"

storiesOf("Touchable", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("is Touchable", () => (
    <Story>
      <UseCase text="Primary" usage="The primsaary.">
        <Touchable onPress={console.warn("it works")}>
          <Text>qweqweqwe</Text>
        </Touchable>
      </UseCase>
    </Story>
  ))
