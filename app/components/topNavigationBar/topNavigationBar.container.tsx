import { TopNavigationBar } from "./topNavigationBar"
import { ArrowIosBackFill } from "@app/assets/icons"
import { isRootRoute, getCurrentRouteIndex, getCurrentRouteState } from "@app/navigation/util"
import React from "react"

export const TopBarContainer = {
  header: props => {
    // @ts-ignore (private API)
    const index: number = getCurrentRouteIndex(props.navigation)
    const screenTitle = props.scene.descriptor.options.title
    const screenSubTitle = props.scene.descriptor.options.subtitle

    return (
      <TopNavigationBar
        {...props}
        title={screenTitle}
        subtitle={screenSubTitle}
        backIcon={isRootRoute(index) && ArrowIosBackFill}
        onBackPress={() => {
          props.navigation.goBack(null)
        }}
      />
    )
  },
}
