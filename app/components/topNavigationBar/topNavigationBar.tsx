import React from "react"
import { StyleType, ThemeType, withStyles } from "react-native-ui-kitten/theme"
import { ImageProps } from "react-native"
import {
  TopNavigation,
  TopNavigationAction,
  TopNavigationActionProps,
  TopNavigationProps,
} from "react-native-ui-kitten/ui"
import { SafeAreaView, NavigationScreenProps } from "react-navigation"
import { fonts } from "@app/theme"
import { Avatar } from "../avatar"
import { AuthModel } from "@app/models/auth"

export interface ComponentProps {
  backIcon?: BackIconProp
  onBackPress?: () => void
}

export type TopNavigationBarProps = TopNavigationProps & ComponentProps & NavigationScreenProps

type BackIconProp = (style: StyleType) => React.ReactElement<ImageProps>
type BackButtonElement = React.ReactElement<TopNavigationActionProps>

class TopNavigationBarComponent extends React.Component<TopNavigationBarProps> {
  private onBackButtonPress = () => {
    if (this.props.onBackPress) {
      this.props.onBackPress()
    }
  }

  private onAvatarPress = () => {
    this.props.navigation.navigate("ProfileScreen", {
      user: AuthModel.getUserName,
    })
  }

  private renderBackButton = (source: BackIconProp): BackButtonElement => {
    return <TopNavigationAction icon={source} onPress={this.onBackButtonPress} />
  }

  private renderAvatar = source => {
    //TODO: add user profile picture here
    return (
      <Avatar
        userImageUrl="http://2.249.89.61:8000/media/posts/sunflower.jpg"
        onPress={this.onAvatarPress}
      />
    )
  }

  public render(): React.ReactNode {
    const { themedStyle, title, subtitle, backIcon } = this.props

    const leftControlElement: BackButtonElement | null = backIcon
      ? this.renderBackButton(backIcon)
      : null

    return (
      <SafeAreaView style={themedStyle.safeArea}>
        <TopNavigation
          alignment="center"
          title={title}
          subtitle={subtitle}
          titleStyle={fonts.family.semibold}
          subtitleStyle={fonts.family.regular}
          leftControl={leftControlElement}
          rightControls={this.renderAvatar(backIcon)}
        />
      </SafeAreaView>
    )
  }
}

export const TopNavigationBar = withStyles(TopNavigationBarComponent, (theme: ThemeType) => ({
  safeArea: {
    backgroundColor: theme["background-basic-color-1"],
  },
}))
