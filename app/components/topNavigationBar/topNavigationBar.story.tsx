import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { TopNavigationBar } from "./"

storiesOf("TopNavigationBar", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <TopNavigationBar text="TopNavigationBar" />
      </UseCase>
    </Story>
  ))
