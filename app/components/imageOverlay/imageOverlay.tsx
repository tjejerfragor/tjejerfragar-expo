import * as React from "react"
import { ImageBackground } from "react-native"
import { Props, styles } from "./"

export function ImageOverlay(props: Props) {
  const { image, children, ...rest } = props

  return (
    <ImageBackground source={image} style={styles.image} {...rest}>
      {children}
    </ImageBackground>
  )
}
