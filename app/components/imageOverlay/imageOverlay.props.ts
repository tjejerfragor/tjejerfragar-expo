import { ViewStyle, ImageSourcePropType } from "react-native"

export interface Props {
  /**
   * Url to image  or file.
   */
  image: ImageSourcePropType

  /**
   * React element to overwrite on image
   */
  children: React.ReactNode

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}
