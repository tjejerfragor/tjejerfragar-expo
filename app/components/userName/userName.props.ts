import { ViewStyle } from "react-native"

export interface Props {
  userName: string
  userGender: "man" | "woman"

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}
