import * as React from "react"
import { View, ViewStyle } from "react-native"
import { Props } from "./userName.props"
import { Text } from "react-native-ui-kitten"
import { helpers } from "@app/utils/helpers"
import { color } from "@app/theme"

export function UserName(props: Props) {
  const { userName, userGender, style, ...rest } = props
  const genderBasedColor = helpers.isMale(userGender)
    ? color.palette.maleGender
    : color.palette.femaleGender
  return (
    <View style={style} {...rest}>
      <Text style={{ color: genderBasedColor, fontSize: 14 }} appearance="hint">
        {userName}
      </Text>
    </View>
  )
}
