import { StyleSheet } from "react-native"
import { color } from "../../theme"

export default StyleSheet.create({
  buttonText: {
    padding: 3,
    borderRadius: 2,
    fontSize: 10,
    backgroundColor: color.palette.steel,
    width: "auto",
    maxWidth: 100,
    textAlign: "center",
  },
})
