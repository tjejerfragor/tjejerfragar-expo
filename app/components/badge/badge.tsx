import * as React from "react"
import { View, ViewStyle, Text } from "react-native"
import BadgeProps from "./badge.props"
import styles from "./badge.style"

export function Badge(props: BadgeProps) {
  const { name, ...rest } = props

  return (
    <View>
      <Text style={styles.buttonText}>{name}</Text>
    </View>
  )
}
