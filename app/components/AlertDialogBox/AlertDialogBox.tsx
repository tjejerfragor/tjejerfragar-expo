import React, { Component } from "react"
import { Props, State } from "./"
import { Button } from "react-native-ui-kitten"
import { AlertIconOutline, QuestionMarkCircleOutline } from "@app/assets/icons"

export class AlertDialogBox extends Component<Props, State> {
  render() {
    const { text, isError } = this.props
    const Icon = isError ? AlertIconOutline : QuestionMarkCircleOutline
    return (
      <Button icon={Icon} disabled={true} appearance="ghost" size="giant">
        {text}
      </Button>
    )
  }
}
