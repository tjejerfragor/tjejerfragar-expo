export interface Props {
  /**
   * An error text
   */
  text: string

  /**
   * Is it error?
   */
  isError: boolean
}
