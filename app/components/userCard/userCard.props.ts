import { Moment } from "moment"
import { ViewStyle } from "react-native"

export default interface UserCardProps {
  maleCommentCount?: number
  femaleCommentCount?: number
  username: string
  userbadge: string
  userImage: string
  userGender: string
  postDate: Moment | Date
  style?: ViewStyle
  onPress: () => void
}
