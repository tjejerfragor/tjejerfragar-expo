import * as React from "react"
import { View, Image } from "react-native"
import styles from "./userCard.styles"
import { fonts, color } from "../../theme"
import { Badge } from "../badge"
import { Touchable } from "../Touchable"
import UserCardProps from "./userCard.props"
import { Text } from "react-native-ui-kitten"
import moment from "moment"
import { MessageCircleIconOutline } from "@app/assets/icons"
import { Avatar } from "../avatar"

function renderPostDetailsSection(maleCommentCount: number, femaleCommentCount: number) {
  if (typeof maleCommentCount !== "undefined") {
    return (
      <View style={styles.postDetails}>
        <Text>
          <Text>
            {MessageCircleIconOutline({
              width: 25,
              height: 25,
              tintColor: color.palette.lightGrey,
              marginTop: 5,
            })}
          </Text>

          <Text appearance="hint" style={styles.organizeCounters}>
            <Text style={styles.maleCommentCounter}> {maleCommentCount} </Text>
            |
            <Text style={styles.femaleCommentCounter}> {femaleCommentCount} </Text>
          </Text>
        </Text>
      </View>
    )
  } else {
    return null
  }
}

export function UserCard(props: UserCardProps) {
  const userGender = props.userGender !== "man"
  const {
    username,
    userbadge,
    userImage,
    maleCommentCount,
    femaleCommentCount,
    postDate,
    style,
    onPress,
  } = props
  const formattedDate: string = moment(postDate)
    .startOf("day")
    .fromNow()

  return (
    <Touchable onPress={onPress}>
      <View style={style}>
        <View style={styles.userDetailsContainer}>
          <Avatar userImageUrl={userImage} />
          <Text style={[styles.userName, fonts.family.bold]}>{username}</Text>
          <Text style={styles.userBadge} category="s2" appearance="hint">
            {formattedDate}
          </Text>
          {renderPostDetailsSection(maleCommentCount, femaleCommentCount)}
        </View>
      </View>
    </Touchable>
  )
}
