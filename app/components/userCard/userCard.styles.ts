import { StyleSheet } from "react-native"
import { color } from "@app/theme"

export default StyleSheet.create({
  userDetailsContainer: {
    flex: 1,
    flexDirection: "row",
    marginTop: 30,
  },
  userImage: {
    width: 30,
    height: 30,
    borderRadius: 30,
    overflow: "hidden",
  },
  userName: {
    marginLeft: 20,
  },
  userBadge: {
    marginTop: 25,
    position: "absolute",
    left: 45,
  },
  postDetails: {
    position: "absolute",
    right: 10,
  },
  maleCommentCounter: {
    color: color.palette.maleGender,
  },
  femaleCommentCounter: {
    color: color.palette.femaleGender,
  },
})
