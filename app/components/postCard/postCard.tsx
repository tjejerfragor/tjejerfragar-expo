import PostCardProps from "./postCard.props"
import { Layout, Text } from "react-native-ui-kitten/ui"
import React from "react"
import { View, Image, Button } from "react-native"
import { fonts, color } from "@app/theme"
import { Screen } from "../screen"
import { ThemeType, withStyles } from "react-native-ui-kitten/theme"
import { Touchable } from "../Touchable"
import { styles } from "./postCard.styles"
import { UserCard } from "../userCard"
import { withNavigation } from "react-navigation"
import stripTags from "striptags"

class _PostCard extends React.Component<PostCardProps> {
  constructor(props) {
    super(props)

    this.onPressUserCard = this.onPressUserCard.bind(this)
  }

  stripHtml(html: string): string {
    return stripTags(html)
  }

  onPressUserCard = () => {
    const { username } = this.props

    this.props.navigation.navigate("ProfileScreen", { user: username })
  }

  render() {
    const {
      onPress,
      postImage,
      category,
      date,
      body,
      username,
      userbadge,
      userImage,
      userGender,
      title,
      themedStyle,
    } = this.props

    const genderBasedColor =
      userGender === "woman" ? color.palette.femaleGender : color.palette.maleGender

    return <Screen style={themedStyle.container}>
        <Touchable onPress={onPress}>
          <Layout style={styles.container}>
            <View>
              <Image source={{ uri: postImage }} style={[styles.postImage, { borderColor: genderBasedColor }]} />
            </View>
            <View style={[styles.container, themedStyle.infoContainer]}>
              <Text style={fonts.family.bold} category="h5">
                {title}
              </Text>
              <Text appearance="hint" category="s1">
                {this.stripHtml(body)}
              </Text>

              <UserCard username={username} userGender={userGender} userbadge={userbadge} userImage={userImage} postDate={date} onPress={() => this.onPressUserCard()} />
            </View>
          </Layout>
        </Touchable>
      </Screen>
  }
}
export const PostCard = withNavigation(
  withStyles(_PostCard, (theme: ThemeType) => ({
    container: {
      backgroundColor: theme["background-basic-color-2"],
    },
    infoContainer: {
      paddingHorizontal: 16,
      paddingVertical: 24,
      borderBottomWidth: 1,
      borderBottomColor: theme["border-basic-color-2"],
    },
  })),
)
