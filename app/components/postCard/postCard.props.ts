import { NavigationScreenProps } from "react-navigation"

export default interface postCardProps extends NavigationScreenProps {
  title: string
  body: string
  postImage: string
  onPress: any
  key: string
  username: string
  userbadge: string
  userImage: string
  userGender: string
  category: string
  date: Date
  themedStyle: any
}
