import { StyleSheet } from "react-native"
import { color, fonts } from "../../theme"

const PostCardStyles = {
  container: {
    borderRadius: 10,
    margin: 15,
  },
  postImage: {
    height: 200,
    borderRadius: 10,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,

    borderWidth: 2,
    borderBottomWidth: 100,
  },
  postTitle: {
    marginTop: 13,
  },
  postBody: {
    marginTop: 13,
    marginBottom: 13,
  },
  content: {
    fontSize: fonts.size.small,
    fontWeight: "bold",
    // color: color.dim
  },
  textContainer: {
    padding: 20,
  },
}
// Make info color gray
const styles = StyleSheet.create(PostCardStyles)

export { PostCardStyles, styles }
