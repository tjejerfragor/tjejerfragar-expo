import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { LoadingSpinner } from "../loadingSpinner"

storiesOf("LoadingSpiner", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Loading activity indicator" usage="Use it via if else condition.">
        <LoadingSpiner isLoading={true} />
      </UseCase>
    </Story>
  ))
