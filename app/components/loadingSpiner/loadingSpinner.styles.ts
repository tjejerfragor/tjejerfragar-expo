import { StyleSheet } from "react-native"

export default StyleSheet.create({
  loadingSpinner: {
    flex: 1,
    justifyContent: "center",
  },
})
