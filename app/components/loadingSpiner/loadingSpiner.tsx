import React from "react"
import { ActivityIndicator, View } from "react-native"
import styles from "./loadingSpinner.styles"
import { color } from "@app/theme"
import { Props } from "./"

export function Loading(props: Props): React.ReactNode {
  const { refreshing } = props
  return (
    <View style={styles.loadingSpinner}>
      <ActivityIndicator size="large" color={color.purple} animating={refreshing} />
    </View>
  )
}
