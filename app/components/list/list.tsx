import * as React from "react"
import { List as L, ListItem, Layout, Text } from "react-native-ui-kitten"
import { Dimensions, View, ImageProps } from "react-native"
import { Props } from "./list.props"
import { Avatar } from "../avatar"
import { IconProps } from "../icon/icon.props"
import { UserName } from "../userName"

const renderItem = ({ item }) => {
  if (item.profilePicture) {
    return (
      <ListItem title={item.name} onPress={item.onPress}>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <Avatar userImageUrl={item.profilePicture} />
          <UserName userName={item.name} userGender={item.userGender} style={{ marginLeft: 20 }} />
        </View>
      </ListItem>
    )
  } else {
    return <ListItem title={item.name} icon={item.icon} onPress={item.onPress} />
  }
}

export function List(props: Props) {
  const { list, style, ...rest } = props

  return (
    <Layout style={[{ height: Dimensions.get("window").height }, style]}>
      <L data={list} renderItem={renderItem} />
    </Layout>
  )
}
