import { ViewStyle } from "react-native"

export interface Props {
  list: Array<object>
  style?: ViewStyle
}
