import { _Post } from '@app/services/api';
import { ViewStyle } from 'react-native';

export interface Props {
  entry: _Post
  buttonStyle?: ViewStyle
}