import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { SharingMenu } from "./"

declare var module

storiesOf("SharingMenu", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <SharingMenu text="SharingMenu" />
      </UseCase>
    </Story>
  ))
