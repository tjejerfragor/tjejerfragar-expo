import { StyleSheet } from "react-native"
import { fonts } from "@app/theme"

export default StyleSheet.create({
  shareButton: { flexDirection: "row", height: 50, ...fonts.family.semibold, marginTop: 20 },
  openActionSheetBtn: {
    width: 100,
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    paddingTop: 5,
    justifyContent: "center",
    flexDirection: "column",
  },
})
