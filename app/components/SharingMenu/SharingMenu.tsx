import * as React from "react"
import { View, Share } from "react-native"
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet"
import { ShareFill, BookmarkFill } from "@app/assets/icons"
import { Text } from "react-native"
import Style from "./SharingMenu.style"
import { Props } from "./"
import { Button } from "react-native-ui-kitten/ui"
import { _Post } from "@app/services/api"
import { frontendUrl } from "../../../app.config.json"
const showActionSheet = () => {
  this.ActionSheet.show()
}

const actionSheetOptions = [
  <Text style={Style.shareButton}>
    {ShareFill({ width: 20, height: 20 })} Dela inlägget via...
  </Text>,
  <Text style={Style.shareButton}>
    {BookmarkFill({ width: 20, height: 20 })} Spara inlägget för senare
  </Text>,
  "Stäng fönstret",
]

const _onPressActionSheetBtn = async (index: number, entry: _Post) => {
  switch (index) {
    case 0:
      const shareUrl = `${frontendUrl}/${entry.slug}`
      await Share.share({ message: shareUrl })
      break
    case 1:
      console.warn("Sparad inlägg.")
      break
  }
}

export function SharingMenu(props: Props) {
  const { entry, buttonStyle } = props
  return (
    <View>
      <Button
        icon={ShareFill}
        style={Style.openActionSheetBtn}
        appearance="ghost"
        size="large"
        textStyle={buttonStyle}
        onPress={showActionSheet}
      />
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        options={actionSheetOptions}
        destructiveButtonIndex={2}
        cancelButtonIndex={2}
        onPress={index => _onPressActionSheetBtn(index, entry)}
      />
    </View>
  )
}
