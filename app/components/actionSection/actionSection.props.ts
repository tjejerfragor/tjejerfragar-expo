import { _Post } from '@app/services/api';

export interface Props {
  postFemaleLikeCount: number
  postMaleLikeCount: number
  onPressLikeButton: any
  entryAlreadyLiked: boolean
  entryToBeShared: _Post
  themedStyle: any
}
