import React, { Component } from "react"
import { View, Share, ImageProps } from "react-native"
import styles from "./actionSection.styles"
import { color, fonts } from "@app/theme/"
import { Props } from "./"
import { Text, Button } from "react-native-ui-kitten/ui"
import { HeartFill, BookmarkFill } from "@app/assets/icons"
import { ThemeType, withStyles } from "react-native-ui-kitten/theme"
import { SharingMenu } from '../SharingMenu';

export class _ActionSection extends Component<Props> {
  render() {
    const {
      entryAlreadyLiked,
      postFemaleLikeCount,
      onPressLikeButton,
      postMaleLikeCount,
      entryToBeShared,
      themedStyle,
    } = this.props

    const likeButtonAppearance = entryAlreadyLiked ? "filled" : "ghost"
    const likeButtonStatus = entryAlreadyLiked ? "danger" : undefined

    return (
      <View style={styles.actionSection}>
        <Button
          icon={BookmarkFill}
          style={themedStyle.button}
          textStyle={themedStyle.buttonText}
          appearance="ghost"
          style={{ position: "absolute", left: 0 }}
          size="large"
        />

        <Text style={[styles.counters, { color: color.palette.femaleGender }]}>
          {postFemaleLikeCount}
        </Text>
        <View style={styles.likeButtonCopy}>
          <Button
            appearance={likeButtonAppearance}
            size="large"
            status={likeButtonStatus}
            icon={HeartFill}
            style={[styles.likeButtonCopy]}
            onPress={onPressLikeButton}
          />
        </View>
        <Text style={[styles.counters, { color: color.palette.maleGender }]}>
          {postMaleLikeCount}
        </Text>

      <SharingMenu entry={entryToBeShared} buttonStyle={themedStyle.buttonText}/>
      </View>
    )
  }
}

export const ActionSection = withStyles(_ActionSection, (theme: ThemeType) => ({
  buttonText: {
    ...fonts.family.light,
    fontSize: fonts.size.medium,
    color: theme["color-primary-600"],
  },
  button: {
    display: "flex",
    flexDirection: "row",
  },
}))
