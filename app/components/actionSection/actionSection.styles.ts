import { StyleSheet } from "react-native"
import { Dimensions } from "react-native"
import { color, fonts } from "@app/theme"

export default StyleSheet.create({
  actionSection: {
    flexDirection: "row",
    justifyContent: "center",
    maxWidth: Dimensions.get("screen").window,
  },
  likeButton: {
    width: 100,
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    paddingTop: 5,
    justifyContent: "center",
    flexDirection: "column",
  },
  likeButtonCopy: {
    width: 50,
    height: 50,
    borderRadius: 500,
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    // paddingTop: 10,
    justifyContent: "center",
    flexDirection: "column",
  },
  counters: {
    padding: 10,
    marginTop: 3,
  },
  buttonText: {
    ...fonts.family.light,
    color: color.text,
    fontSize: fonts.size.medium,
  },
})
