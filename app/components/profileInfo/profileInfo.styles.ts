import { StyleSheet } from "react-native"
import { fonts } from "@app/theme"

export const Style = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  detailsContainer: {},
  nameLabel: fonts.family.bold,
  locationLabel: {
    marginTop: 8,
  },
  profileAvatar: {
    width: 64,
    height: 64,
  },
})
