import { ViewStyle } from "react-native"

export interface Props {
  userName: string
  style?: ViewStyle
}
