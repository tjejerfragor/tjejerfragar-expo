import * as React from "react"
import { View } from "react-native"
import { Props, Style } from "./"
import { Text } from "react-native-ui-kitten"

export function ProfileInfo(props: Props) {
  const { userName, style } = props

  return (
    <View style={[Style.container, style]}>
      <View style={Style.detailsContainer}>
        <Text style={Style.nameLabel} category="h6">
          {userName}
        </Text>
        <Text appearance="hint" category="s1">
          Kral
        </Text>
      </View>
    </View>
  )
}
