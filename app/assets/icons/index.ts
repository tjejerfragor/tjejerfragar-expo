import { ImageStyle, StyleProp } from "react-native"
import { Icon, IconElement, IconSource } from "./icon.component"

export const MessageCircleIconOutline = (style: StyleProp<ImageStyle>) => {
  const source: IconSource = {
    imageSource: require("./eva/message-circle-outline.png"),
  }

  return Icon(source, style)
}

export const SettingsOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/settings-outline.png"),
  }

  return Icon(source, style)
}

export const HomeOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/home-outline.png"),
  }

  return Icon(source, style)
}

export const LogOutOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/log-out-outline.png"),
  }

  return Icon(source, style)
}
export const SunOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/sun-outline.png"),
  }

  return Icon(source, style)
}
export const ArrowIosBackFill = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/arrow-ios-back.png"),
  }

  return Icon(source, style)
}
export const ShareFill = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/share.png"),
  }

  return Icon(source, style)
}
export const HeartFill = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/heart.png"),
  }

  return Icon(source, style)
}
export const BookmarkFill = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/bookmark.png"),
  }

  return Icon(source, style)
}
export const PlusCircleOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/plus-circle-outline.png"),
  }

  return Icon(source, style)
}
export const BulbOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/bulb-outline.png"),
  }

  return Icon(source, style)
}
export const QuestionMarkCircleOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/question-mark-circle-outline.png"),
  }

  return Icon(source, style)
}
export const MessageCircleFill = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/message-circle.png"),
  }

  return Icon(source, style)
}
export const PersonAddFill = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = {
    imageSource: require("./eva/person-add.png"),
  }

  return Icon(source, style)
}

export const AlertIconOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = { imageSource: require("./eva/alert-circle-outline.png") }

  return Icon(source, style)
}

export const EyeOffOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = { imageSource: require("./eva/eye-off-outline.png") }

  return Icon(source, style)
}

export const CheckmarkOutline = (style: StyleProp<ImageStyle>): IconElement => {
  const source: IconSource = { imageSource: require("./eva/checkmark-outline.png") }

  return Icon(source, style)
}

export { Icon, IconSource, RemoteIcon } from "./icon.component"
