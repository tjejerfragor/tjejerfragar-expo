import { _Post } from "@app/services/api"

export interface State {
  post: _Post
  loadingEntry: boolean
  maleCommentCount: number
  femaleCommentCount: number
  postMaleLikeCount: number
  postFemaleLikeCount: number
  entryAlreadyLiked: boolean
}
