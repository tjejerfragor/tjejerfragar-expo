import React, { Component } from "react"
import { Dimensions, Image } from "react-native"
import { UserCard } from "@app/components/userCard"
import { fonts } from "@app/theme"
import { Loading } from "@app/components/loadingSpiner"
import HTMLView from "react-native-render-html"
import { IGNORED_TAGS } from "react-native-render-html/src/HTMLUtils"
import { Props, State } from "./"
import { Api } from "@app/services/api"
import { Text, Layout as View, Layout } from "react-native-ui-kitten"
import { ThemeType, withStyles } from "react-native-ui-kitten/theme"
import { ImageOverlay } from "@app/components/imageOverlay"
import { Avatar } from "@app/components/avatar"
import { TopBarContainer } from "@app/components/topNavigationBar"
import { Screen } from "@app/components/screen"
import { ActionSection } from "@app/components/actionSection/actionSection"
import { frontendUrl } from "../../../app.config.json"
import { helpers } from "@app/utils/helpers"
import stripTags from "striptags"
import { AuthModel } from "@app/models/auth"
import { inject } from "mobx-react"

const ignoredTags = [...IGNORED_TAGS]
const apiClient = new Api()

@inject("authStore")
class _PostDetailScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      post: {},
      loadingEntry: true,
      maleCommentCount: 0,
      femaleCommentCount: 0,
      postMaleLikeCount: 0,
      postFemaleLikeCount: 0,
      entryAlreadyLiked: false,
    }

    this.likeEntry = this.likeEntry.bind(this)
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state

    return {
      title: params.data.title,
      header: TopBarContainer.header,
    }
  }

  async likeEntry() {
    // const formData = {}
    // const postData =
    //   Object.keys(this.state.post).length < 0 || this.props.navigation.getParam("data")
    // const isQuestion = postData.isQuestion
    // if (isQuestion) {
    //   formData["isQuestion"] = postData.id
    // } else {
    //   formData["isPost"] = postData.id
    // }
    // formData["gender"] = this.props.authStore.
    // const { data } = await apiClient.likeEntry(postData.id, formData)
    // if (data.status && data.status === true) {
    //   if (this.props.userGender === "woman") {
    //     this.setState({
    //       postFemaleLikeCount: this.state.postFemaleLikeCount + 1,
    //       entryAlreadyLiked: true,
    //     })
    //   } else {
    //     this.setState({
    //       postMaleLikeCount: this.state.postMaleLikeCount + 1,
    //       entryAlreadyLiked: true,
    //     })
    //   }
    // } else if (data.status === false) {
    //   this.setState({
    //     entryAlreadyLiked: true,
    //   })
    // }
  }

  async unLikeEntry() {
    const formData = {
      isQuestion: null,
      isPost: null,
      isComment: null,
      gender: AuthModel.getUserGender,
    }
    const postData =
      Object.keys(this.state.post).length < 0 || this.props.navigation.getParam("data")
    const isQuestion = postData.isQuestion

    if (isQuestion) {
      formData["isQuestion"] = postData.id
    } else {
      formData["isPost"] = postData.id
    }

    const { data } = await apiClient.unLikeEntry(postData.id, formData, AuthModel.getToken)
    if (data.status && data.status === true) {
      if (this.props.userGender === "woman") {
        this.setState({
          postFemaleLikeCount: this.state.postFemaleLikeCount - 1,
          entryAlreadyLiked: false,
        })
      } else {
        this.setState({
          postMaleLikeCount: this.state.postMaleLikeCount - 1,
          entryAlreadyLiked: false,
        })
      }
    }
  }

  onPressLikeButton() {
    if (this.state.entryAlreadyLiked) {
      return this.unLikeEntry()
    }

    return this.likeEntry()
  }

  async fetchCommentCount() {
    const { data } = await apiClient.getEntryCommentCount(this.state.post.slug)

    let maleComments = 0
    let femaleComments = 0

    for (const comment of data.results) {
      if (comment.user_details_data.gender === "man") {
        maleComments = maleComments + 1
      }
      if (comment.user_details_data.gender === "woman") {
        femaleComments = femaleComments + 1
      }
    }

    this.setState({
      maleCommentCount: maleComments,
      femaleCommentCount: femaleComments,
    })
  }

  async fetchLikeCount() {
    const postData = this.props.navigation.getParam("data")
    const isQuestion = postData.isQuestion || false

    const { data } = await apiClient.getPostLikeCount(isQuestion, postData.id)
    const maleLikeCount = data.results[0].boy_like_count
    const femaleLikeCount = data.results[0].girl_like_count

    this.setState({
      postMaleLikeCount: maleLikeCount,
      postFemaleLikeCount: femaleLikeCount,
    })
  }

  componentDidMount() {
    this.setState({
      post: this.props.navigation.getParam("data"),
      loadingEntry: false,
    })
    this.fetchCommentCount()
    this.fetchLikeCount()

    apiClient
      .isAlreadyLiked({
        token: String(AuthModel.getToken),
        postId: this.props.navigation.getParam("data").id,
      })
      .then(resp => {
        this.setState({
          entryAlreadyLiked: resp.data.liked,
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  stripHtml(html: string): string {
    const allowedTags = ["a", "h1", "h2", "h3", "h4", "h5", "h6", "p", "img"]
    return stripTags(html, allowedTags)
  }

  private onLinkPress(event, href) {
    helpers.openUrl(href)
    return
  }

  private onPressUserCard = () => {
    const { username } = this.state.post.user_data

    this.props.navigation.navigate("ProfileScreen", { user: username })
  }

  render() {
    if (!this.state.loadingEntry) {
      const { profile_picture, gender } = this.state.post.user_details_data
      const { username } = this.state.post.user_data
      const { title, category_data, created_at, content, slug, image } = this.state.post
      const { themedStyle } = this.props
      const strippedContent = this.stripHtml(content)
      return (
        <Screen preset="scroll" style={themedStyle.container}>
          <ImageOverlay image={{ uri: image }}>
            <Avatar
              userImageUrl={profile_picture}
              style={themedStyle.authorPhoto}
              avatarSize="large"
            />
          </ImageOverlay>

          <View style={{ margin: 20 }}>
            <Text category="h5" style={themedStyle.titleLabel}>
              {title}
            </Text>

            <HTMLView
              html={strippedContent}
              onLinkPress={this.onLinkPress}
              ignoredTags={ignoredTags}
              tagsStyles={themedStyle}
            />

            <UserCard
              username={username}
              userbadge="Adamin Dibi"
              userImage={profile_picture}
              userGender={gender}
              maleCommentCount={this.state.maleCommentCount}
              femaleCommentCount={this.state.femaleCommentCount}
              style={themedStyle.detailsContainer}
              postDate={created_at}
              onPress={this.onPressUserCard}
            />

            <ActionSection
              postFemaleLikeCount={this.state.postFemaleLikeCount}
              postMaleLikeCount={this.state.postMaleLikeCount}
              onPressLikeButton={() => this.onPressLikeButton()}
              entryAlreadyLiked={this.state.entryAlreadyLiked}
              entryToBeShared={this.state.post}
            />
          </View>
        </Screen>
      )
    } else {
      return <Loading refreshing={this.state.loadingEntry} />
    }
  }
}

export const PostDetailScreen = withStyles(_PostDetailScreen, (theme: ThemeType) => ({
  container: {
    display: "flex",
    backgroundColor: theme["background-basic-color-1"],
    minHeight: Dimensions.get("window").height,
  },
  p: {
    color: theme["text-basic-color"],
    lineHeight: 20,
    marginVertical: 10,
  },
  h2: {
    color: theme["text-basic-color"],
  },
  h3: {
    color: theme["text-basic-color"],
    fontSize: fonts.size.h6,
    margin: 10,
  },
  h4: {
    color: theme["text-basic-color"],
  },
  h5: {
    color: theme["text-basic-color"],
  },
  h6: {
    color: theme["text-basic-color"],
  },
  b: {
    color: theme["text-basic-color"],
  },
  img: {
    justifyContent: "center" /* align horizontal */,
    alignItems: "center" /* align vertical */,
    maxWidth: 300,
  },
  detailsContainer: {
    paddingHorizontal: 24,
    paddingVertical: 24,
    borderTopWidth: 1,
    borderTopColor: theme["border-basic-color-2"],
  },
  dateContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    minHeight: 175,
  },
  authorPhoto: {
    marginTop: 130,
    marginHorizontal: 24,
  },
  titleLabel: {
    marginVertical: 30,
    ...fonts.family.bold,
  },
}))
