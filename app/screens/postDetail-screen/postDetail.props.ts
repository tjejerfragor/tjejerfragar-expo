import { NavigationScreenProps } from "react-navigation"
import { Auth } from '@app/models/auth';

export interface Props extends NavigationScreenProps {
  userGender: string,
  authStore?: Auth
}
