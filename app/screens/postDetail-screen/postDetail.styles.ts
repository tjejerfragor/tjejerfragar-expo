import { StyleSheet } from "react-native"
import { fonts } from "@app/theme"

export default StyleSheet.create({
  cc: {
    padding: 20,
  },
  title: {
    marginVertical: 10,
    ...fonts.style.h5,
    fontWeight: "bold",
  },

  userInfo: {
    marginVertical: 20,
  },

  infoSection: {
    display: "flex",
    alignItems: "stretch",
    flexDirection: "row",
  },

  infoRightSide: {
    marginLeft: "auto",
  },
})
