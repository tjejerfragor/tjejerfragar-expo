import React from "react"
import { NavigationScreenProps } from "react-navigation"
import { Dimensions, Alert } from "react-native"
import { LogOutOutline, SunOutline } from "@app/assets/icons"
import { MainModel } from "@app/models/main"
import { TopBarContainer } from "@app/components/topNavigationBar"
import { List } from "@app/components/list"
import { reset as KeychainDelete } from "@app/utils/keychain"

export default class SettingsScreen extends React.Component<NavigationScreenProps> {
  constructor(props) {
    super(props)
    this.logout = this.logout.bind(this)
    this.changeTheme = this.changeTheme.bind(this)
  }

  settings = [
    {
      name: "Logga ut",
      icon: LogOutOutline,
      onPress: () => this.logout(),
    },
    {
      name: "Ändra tema",
      icon: SunOutline,
      onPress: () => this.changeTheme(),
    },
  ]

  changeTheme = () => {
    MainModel.toggleTheme()
    this.props.navigation.navigate("mainScreen", { user: this.props.navigation.getParam("user") })
  }

  logout() {
    KeychainDelete("user").then(() => {
      this.props.navigation.navigate("loginScreen")
      return
    })
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Inställningar",
      header: TopBarContainer.header,
    }
  }

  render() {
    return <List list={this.settings} />
  }
}
