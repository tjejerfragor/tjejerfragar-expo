import { NavigationScreenProps } from "react-navigation"

export interface Props extends NavigationScreenProps<{}> {}
