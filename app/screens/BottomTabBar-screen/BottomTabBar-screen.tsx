import * as React from "react"
import { observer } from "mobx-react"
import { Props } from "./"
import { BottomNavigation, BottomNavigationTab } from "react-native-ui-kitten"
import {
  SettingsOutline,
  HomeOutline,
  QuestionMarkCircleOutline,
  BulbOutline,
} from "@app/assets/icons"

@observer
export class BottomTabBarScreen extends React.Component<Props, {}> {
  onTabSelect = (index: number) => {
    const { navigation } = this.props
    const { [index]: selectedRoute } = navigation.state.routes

    navigation.navigate({
      routeName: selectedRoute.routeName,
    })
  }

  render() {
    return (
      <BottomNavigation
        selectedIndex={this.props.navigation.state.index}
        onSelect={this.onTabSelect}
      >
        <BottomNavigationTab title="Showcase" icon={HomeOutline} />
        <BottomNavigationTab title="Frågor" icon={QuestionMarkCircleOutline} />
        <BottomNavigationTab title="mittAnta" icon={BulbOutline} />
        <BottomNavigationTab title="Inställningar" icon={SettingsOutline} />
      </BottomNavigation>
    )
  }
}
