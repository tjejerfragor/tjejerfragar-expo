import * as React from "react"
import { observer, inject } from "mobx-react"
import { Alert, View, KeyboardAvoidingView } from "react-native"
import { fonts } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Api } from "../../services/api"
import { AuthModel, Auth } from "../../models/auth"
import { save as KeychainSave, load as KeychainLoad } from "../../utils/keychain"
import { ImageOverlay } from "@app/components/imageOverlay"
import { withStyles, ThemeType, ThemedComponentProps } from "react-native-ui-kitten/theme"
import { Text, Input, Button } from "react-native-ui-kitten"
import { PersonAddFill, EyeOffOutline } from "@app/assets/icons"
import { helpers } from "@app/utils/helpers"

const apiClient = new Api()

type NeededProps = NavigationScreenProps & ThemedComponentProps

interface LoginScreenProps extends NeededProps {
  authStore?: Auth
}

interface State {
  email: string
  password: string
  startLogin: boolean
}

@inject("authStore")
@observer
export class _LoginScreen extends React.Component<LoginScreenProps, State> {
  constructor(props: LoginScreenProps) {
    super(props)

    this.loginBtn = this.loginBtn.bind(this)
  }

  state = {
    email: "",
    password: "",
    startLogin: false,
  }

  static navigationOptions = {
    header: null,
  }

  //TODO: Save only token to keychain
  loginBtn = async () => {
    this.setState({ startLogin: true })
    const { email, password } = this.state
    const { authStore } = this.props
    const trimmedEmail = email.trim()
    const trimmedPassword = password.trim()

    const login = await apiClient.login(trimmedEmail, trimmedPassword)
    const token = login.data.token

    if (token) {
      const userInformation = await apiClient.getUserInformation(token)
      const user = { ...userInformation.data.user, token }

      try {
        authStore.setUserData(user)
        authStore.setToken(token)
      } catch (e) {
        console.error(e)
      }

      try {
        /**
         * Saving user data to keychain.
         */
        KeychainSave("user", JSON.stringify(user))
      } catch (err) {
        Alert.alert("err", err)
      }

      return this.props.navigation.navigate("mainScreen", { user: AuthModel.userData })
    } else {
      this.setState({ startLogin: false })
      Alert.alert("", "Det gick inte att logga in med angivna uppgifter inloggningsuppgifter.")
    }
  }

  private onEmailInputChanged = (email: string) => {
    this.setState({ email })
  }

  private onPasswordInputChanged = (password: string) => {
    this.setState({ password })
  }

  private onPressForgotPasswordBtn = () => {
    helpers.openUrl("http://2.249.89.61:3000/")

    return
  }

  private onPressSignupBtn = () => {
    helpers.openUrl("http://2.249.89.61:3000/")
    return
  }

  async componentDidMount() {
    const _user = await KeychainLoad("user")
    const user = JSON.parse(_user)
    const { authStore } = this.props

    if (user) {
      try {
        authStore.setUserData(user)
        authStore.setToken(user.token)
      } catch (err) {
        console.error(err)
      }

      return this.props.navigation.navigate("mainScreen", { user: user })
    }
  }
  render() {
    const { themedStyle } = this.props

    return (
      <ImageOverlay
        style={themedStyle.container}
        image={{
          uri:
            "http://www.bestfunforall.com/better/imgs/12%20girl%20and%20man%20in%20love%20on%20beach%20wallpaper%20%2010.jpg",
        }}
      >
        <View style={themedStyle.headerContainer}>
          <Text style={themedStyle.helloLabel} category="h1">
            Hej!
          </Text>
          <Text style={themedStyle.signInLabel} category="s1">
            Logga in till ditt konto.
          </Text>
        </View>

        <View style={themedStyle.formContainer}>
          <KeyboardAvoidingView>
            <Input
              value={this.state.email}
              onChangeText={this.onEmailInputChanged}
              placeholder="Email"
              icon={PersonAddFill}
              textContentType="emailAddress"
              textStyle={themedStyle.paragraph}
              autoCompleteType="email"
              autoCapitalize="none"
            />
            <Input
              value={this.state.password}
              onChangeText={this.onPasswordInputChanged}
              placeholder="Lösenord"
              secureTextEntry={true}
              textContentType="password"
              icon={EyeOffOutline}
              textStyle={themedStyle.paragraph}
              style={themedStyle.passwordInput}
              onSubmitEditing={this.loginBtn}
            />

            <Button
              style={themedStyle.signInButton}
              textStyle={fonts.family.bold}
              size="giant"
              disabled={!this.state.email || !this.state.password}
              onPress={this.loginBtn}
            >
              {this.state.startLogin ? "LOGGAR IN.." : "LOGGA IN"}
            </Button>
          </KeyboardAvoidingView>
          <Button
            style={themedStyle.signUpButton}
            textStyle={themedStyle.signUpText}
            appearance="ghost"
            activeOpacity={0.75}
            onPress={this.onPressSignupBtn}
          >
            Har du inte konto än?
          </Button>
          <View style={themedStyle.forgotPasswordContainer}>
            <Button
              style={themedStyle.forgotPasswordButton}
              textStyle={themedStyle.forgotPasswordText}
              appearance="ghost"
              activeOpacity={0.75}
              onPress={this.onPressForgotPasswordBtn}
            >
              Glömt lösenordet?
            </Button>
          </View>
        </View>
      </ImageOverlay>
    )
  }
}

export const LoginScreen = withStyles(_LoginScreen, (theme: ThemeType) => ({
  container: {
    flex: 1,
  },
  headerContainer: {
    minHeight: 216,
    justifyContent: "center",
    alignItems: "center",
  },
  passwordInput: {
    marginTop: 0,
  },
  formContainer: {
    flex: 1,
    justifyContent: "space-between",
    paddingHorizontal: 16,
  },
  helloLabel: {
    color: "white",
    ...fonts.family.bold,
  },
  signInLabel: {
    marginTop: 16,
    color: "white",
    ...fonts.family.semibold,
  },
  signInButton: {
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 20,
  },
  signUpText: {
    color: "white",
    ...fonts.family.semibold,
  },
  forgotPasswordText: {
    fontSize: 15,
    color: theme["text-hint-color"],
    ...fonts.family.semibold,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
}))
