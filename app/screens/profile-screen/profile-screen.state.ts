import { _User, _Question, Result as Follow } from "@app/services/api"

export interface State {
  //TODO: add custom type
  userInformation: _User
  followersCount: number
  followers: Follow[]
  following: Follow[]
  questions: _Question
  disableActionButtons: boolean
  alreadyFollowingUser: boolean
}
