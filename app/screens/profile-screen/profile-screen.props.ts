import { ThemedComponentProps } from "react-native-ui-kitten/theme"
import { NavigationScreenProps } from "react-navigation"
import { Auth } from "@app/models/auth"
import { Follow } from "@app/models/follow"

interface ComponentsProps {
  authStore?: Auth
  followStore?: Follow
}

export type Props = ThemedComponentProps & NavigationScreenProps & ComponentsProps
