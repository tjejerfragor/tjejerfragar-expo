import * as React from "react"
import { List } from "@app/components/list"
import { NavigationScreenProps } from "react-navigation"
import { TopBarContainer } from "@app/components/topNavigationBar"
import { AlertDialogBox } from "@app/components/AlertDialogBox"
import { Layout } from "react-native-ui-kitten"
import { Screen } from "@app/components/screen"
import { withStyles, ThemeType, ThemedComponentProps } from "react-native-ui-kitten/theme"
import { LoadingSpinner } from "@app/components/loadingSpinner"

type BootstrapProps = NavigationScreenProps & ThemedComponentProps

interface Props extends BootstrapProps {
  followers: Array<object>
}

interface State {}

interface Item extends Object {
  name: string
  profilePicture?: string
  icon?: any
  userGender?: "man" | "woman"
  onPress?: () => void
}

class _UserFollowers extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      //TODO: add dynamic subtitle that counts gender of total user
      title: "Följare",
      subtitle: navigation.getParam("subtitle") || "2 Tjejer | 1 Kille",
      header: TopBarContainer.header,
    }
  }

  list: Array<Item> = []

  initFollowersList(): Array<Item> {
    const followersList: Array<object & Item> = []
    const followersFromNavigation = this.props.navigation.getParam("userFollowers")

    for (const user of followersFromNavigation) {
      const userDetails: Item = {
        name: user.user_details.user_data.username,
        profilePicture: user.user_details.profile_picture,
        userGender: user.user_details.gender,
        onPress: () => {
          this.props.navigation.navigate("mainScreen")
          this.props.navigation.navigate("ProfileScreen", {
            user: user.user_details.user_data.username,
          })
        },
      }

      followersList.push(userDetails)
    }

    return followersList
  }

  initFollowingList(): Array<Item> {
    const followersList: Array<object & Item> = []
    const followersFromNavigation = this.props.navigation.getParam("userFollowers")

    for (const user of followersFromNavigation) {
      const userDetails: Item = {
        name: user.actor_details.user_data.username,
        profilePicture: user.actor_details.profile_picture,
        userGender: user.actor_details.gender,
        onPress: () => {
          this.props.navigation.navigate("mainScreen")
          this.props.navigation.navigate("ProfileScreen", {
            user: user.actor_details.user_data.username,
          })
        },
      }

      followersList.push(userDetails)
    }

    return followersList
  }
  componentWillMount() {
    const isFollowingList = this.props.navigation.getParam("following")

    if (!isFollowingList) {
      this.list = this.initFollowersList()
    } else {
      this.list = this.initFollowingList()
    }
  }

  render() {
    const followersFromNavigation = this.props.navigation.getParam("userFollowers")

    if (followersFromNavigation.length < 1) {
      return (
        <Screen preset="fixed" style={this.props.themedStyle.container}>
          <Layout>
            <AlertDialogBox
              text="Här finns inget som kan visas! Säg till att du följer nån."
              isError={false}
            />
          </Layout>
        </Screen>
      )
    } else {
      if (this.list.length > 0) {
        return <List list={this.list} />
      } else {
        return <LoadingSpinner isLoading={true} />
      }
    }
  }
}

export const UserFollowers = withStyles(_UserFollowers, (theme: ThemeType) => ({
  container: {
    backgroundColor: theme["background-basic-color-1"],
  },
}))
