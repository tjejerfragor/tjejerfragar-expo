import React from "react"
import { List } from "@app/components/list"
import { _Question } from "@app/services/api"
import { NavigationScreenProps } from "react-navigation"
import { TopBarContainer } from "@app/components/topNavigationBar"

/**
 * Navigation Params:
 * questions
 */

interface Props extends NavigationScreenProps {}

UserQuestions.navigationOptions = ({ navigation }) => {
  return {
    title: "Frågor",
    header: TopBarContainer.header,
  }
}

function initQuestionsForList(questions: _Question[], navigate): Array<Object> {
  let serializedList: Array<Object> = []

  for (const question of questions) {
    serializedList.push({
      name: question.title,
      profilePicture: question.image,
      userGender: question.user_details_data.gender,
      onPress: () => {
        navigate("postDetailScreen", {
          data: { ...question, isQuestion: true },
        })
      },
    })
  }

  return serializedList
}

export function UserQuestions(props: Props) {
  const { navigation } = props
  const serializedList = initQuestionsForList(navigation.getParam("questions"), navigation.navigate)

  return <List list={serializedList} />
}
