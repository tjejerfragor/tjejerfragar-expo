import * as React from "react"
import { Props, State } from "./"
import { Screen } from "@app/components/screen"
import { ThemeType, withStyles } from "react-native-ui-kitten/theme"
import { ProfileInfo } from "@app/components/profileInfo"
import { fonts } from "@app/theme"
import { View, ScrollView } from "react-native"
import { Button } from "react-native-ui-kitten/ui"
import {
  PersonAddFill,
  MessageCircleFill,
  SettingsOutline,
  CheckmarkOutline,
} from "@app/assets/icons"
import { TopBarContainer } from "@app/components/topNavigationBar"
import { ProfileSocialDetails } from "@app/components/profileSocialDetails"
import { PostCard } from "@app/components/postCard"
import { Api, _Question } from "@app/services/api"
import { AuthModel } from "@app/models/auth"
import { inject } from "mobx-react"
import { LoadingSpinner } from "@app/components/loadingSpinner/loadingSpinner"
import { AppState } from "@app/app"

const apiClient = new Api()

/**
 * Navigation params:
 * user: user object
 */

@inject((stores: AppState) => ({
  authStore: stores.authStore,
  followStore: stores.followStore,
}))
class _ProfileScreen extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.onFollowersPress = this.onFollowersPress.bind(this)
    this.onFollowingPress = this.onFollowingPress.bind(this)
    this.onPostsPress = this.onPostsPress.bind(this)
    this.onSettingsPress = this.onSettingsPress.bind(this)
  }
  static navigationOptions = {
    title: "muhammed",
    header: TopBarContainer.header,
  }

  state: State = {
    userInformation: null,
    followersCount: 0,
    followers: [],
    following: [],
    questions: null,
    disableActionButtons: false,
    alreadyFollowingUser: false,
  }

  private async onFollowPress(userId: number) {
    const token = AuthModel.getToken
    const { data } = await apiClient.followUser(userId, token)

    if (data.status_code === "followed") {
      this.setState({ alreadyFollowingUser: true, followersCount: this.state.followersCount + 1 })
    } else {
      this.setState({ alreadyFollowingUser: true })
    }
  }

  private checkIfFollowingUser(userName: string) {
    const { getFollowings, getFollowers } = this.props.followStore
    const sea = getFollowings.filter(v => v.actor_details.user_data.username === userName)

    if (sea.length > 0) {
      this.setState({
        alreadyFollowingUser: true,
      })
    }
  }

  private async unFollowUser(userId: number) {
    this.setState({
      alreadyFollowingUser: false,
    })

    return
  }

  private onMessagePress() {
    console.warn("you send message")
  }

  private onFollowersPress() {
    const { followers } = this.state
    this.props.navigation.navigate("_userFollowers", {
      userFollowers: followers,
    })
  }

  private onFollowingPress() {
    const { following } = this.state
    console.log(this.props.followStore.getFollowings)
    this.props.navigation.navigate("_userFollowers", { userFollowers: following, following: true })
  }

  private onPostsPress() {
    const { questions } = this.state
    this.props.navigation.navigate("_userQuestions", { questions: questions })
  }

  private _onPressQuestionCard(question: _Question) {
    this.props.navigation.navigate("postDetailScreen", { data: { ...question, isQuestion: true } })
  }

  private async fetchUserQuestions(userId: number) {
    const { data } = await apiClient.getUserQuestions(userId)

    this.setState({ questions: data.results })
  }

  private async fetchUserDetails(userName: string) {
    const { data } = await apiClient.getUserDetails(userName)

    return Promise.resolve(data.results[0])
  }

  private onSettingsPress() {
    this.props.navigation.navigate("SettingsScreen")
  }

  private async fetchUserFollowers(userId: number) {
    const { data } = await apiClient.getFollowers(userId, AuthModel.getToken)
    if (this.state.disableActionButtons) {
      this.props.followStore.setFollowers(data.results)
    }
    this.setState({ followers: data.results })
  }
  private async fetchUserFollowings(userId: number) {
    const { data } = await apiClient.getFollowing(userId, AuthModel.getToken)
    if (this.state.disableActionButtons) {
      this.props.followStore.setFollowings(data.results)
    }
    this.setState({ following: data.results })

    return data.results
  }

  componentDidMount() {
    const { navigation } = this.props
    if (navigation.getParam("user") === AuthModel.getUserName) {
      this.setState({
        disableActionButtons: true,
      })
    }

    this._fetchUserDetails(true, navigation.getParam("user"))
  }

  private _fetchUserDetails(isClientUser: boolean = false, userName?: string) {
    const clientUserName: string = this.props.authStore.getUserName

    if (isClientUser) {
      this.fetchUserDetails(clientUserName).then(async user => {
        const userId = user.user_data.id
        const followings: Array<object> = await this.fetchUserFollowings(userId)

        this.props.followStore.setFollowings(followings)
        this.checkIfFollowingUser(userName)
      })
    }

    this.fetchUserDetails(userName).then(user => {
      const userId = user.user_data.id
      this.setState({ userInformation: user })
      this.fetchUserFollowers(userId)
      this.fetchUserFollowings(userId)
      this.fetchUserQuestions(userId)

      this.checkIfFollowingUser(userId)
    })
  }

  private renderActionButtons(userId: number) {
    const followUserBtnIcon = this.state.alreadyFollowingUser ? CheckmarkOutline : PersonAddFill
    const followUserBtnText = this.state.alreadyFollowingUser ? "Följer" : "Följa"
    const followUserBtnAction = this.state.alreadyFollowingUser
      ? () => this.unFollowUser(userId)
      : () => this.onFollowPress(userId)

    if (!this.state.disableActionButtons) {
      return (
        <View style={this.props.themedStyle.actionContainer}>
          <Button
            textStyle={fonts.family.bold}
            icon={followUserBtnIcon}
            onPress={followUserBtnAction}
          >
            {followUserBtnText}
          </Button>
          <Button
            style={this.props.themedStyle.messageButton}
            textStyle={fonts.family.bold}
            appearance="outline"
            icon={MessageCircleFill}
            onPress={this.onMessagePress}
          >
            SKICKA MEDDELANDE
          </Button>
        </View>
      )
    } else {
      return (
        <View style={this.props.themedStyle.actionContainer}>
          <Button
            textStyle={fonts.family.bold}
            icon={SettingsOutline}
            onPress={this.onSettingsPress}
          >
            Inställningar
          </Button>
        </View>
      )
    }
  }

  render() {
    const { themedStyle } = this.props
    if (this.state.userInformation !== null && this.state.questions !== null) {
      const { username, id } = this.state.userInformation.user_data
      const { followers, following, questions } = this.state
      return (
        <Screen style={themedStyle.container} preset="scroll">
          <ScrollView>
            <ProfileInfo userName={username} style={themedStyle.profileInfo} />

            {this.renderActionButtons(id)}

            <ProfileSocialDetails
              followers={followers.length}
              following={following.length}
              posts={questions !== null ? questions.length : 0}
              style={themedStyle.profileSocials}
              onFollowersPress={this.onFollowersPress}
              onFollowingPress={this.onFollowingPress}
              onPostsPress={this.onPostsPress}
            />

            {questions.map((question: _Question) => (
              <PostCard
                title={question.title}
                body={question.short_content}
                postImage={question.image}
                category={question.category}
                date={question.created_at}
                onPress={() => this._onPressQuestionCard(question)}
                username={question.user_data.username}
                userImage={question.user_details_data.profile_picture}
                userbadge={"Adam"}
                userGender={question.user_details_data.gender}
              />
            ))}
          </ScrollView>
        </Screen>
      )
    } else {
      return <LoadingSpinner isLoading={true} />
    }
  }
}

export const ProfileScreen = withStyles(_ProfileScreen, (theme: ThemeType) => ({
  container: {
    flex: 1,
    backgroundColor: theme["background-basic-color-1"],
  },
  modalContainer: {
    height: 500,
    width: 200,
    zIndex: 50,
    marginLeft: "auto",
  },
  profileInfo: {
    paddingHorizontal: 24,
  },
  actionContainer: {
    flexDirection: "row",
    marginTop: 32,
    paddingHorizontal: 24,
  },
  profileSocials: {
    justifyContent: "space-evenly",
    marginTop: 24,
    paddingVertical: 16,
    borderTopWidth: 1,
    borderTopColor: theme["border-basic-color-2"],
  },
  followButton: {
    flex: 1,
    marginRight: 4,
  },
  messageButton: {
    flex: 1,
    marginLeft: 4,
  },
  activityContainer: {
    paddingVertical: 16,
    backgroundColor: theme["background-basic-color-2"],
  },
  activityList: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  categoryLabel: {
    marginHorizontal: 24,
    marginTop: 8,
    ...fonts.family.semibold,
  },
}))
