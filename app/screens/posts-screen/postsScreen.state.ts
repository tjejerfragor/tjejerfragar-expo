export interface State {
  posts: Array<object>
  refreshing: boolean
}
