import * as React from "react"
import { observer } from "mobx-react"
import { StyleSheet, Text, Button } from "react-native"
import { ScrollView, RefreshControl } from "react-native"
import { Api } from "../../services/api"
import { Props, State } from "./"
import { PostCard } from "@app/components/postCard"
import { TopBarContainer } from "@app/components/topNavigationBar"
import { PostsModel } from "@app/models/posts"

const apiClient = new Api()

// @inject("mobxstuff")
@observer
export class PostsScreen extends React.Component<Props, State> {
  state = {
    entry: [],
    refreshing: true,
  }
  static navigationOptions = {
    title: "Tjejerfrågar",
    header: TopBarContainer.header,
  }

  _onRefresh = () => {
    this.fetchPosts().then(() => {
      this.setState({ refreshing: false })
    })
  }

  _onPressProfilePicture = () => {
    this.props.navigation.navigate("SettingsScreen")
  }

  _onPress = data => {
    this.props.navigation.navigate("postDetailScreen", { data: data, routeName: data.title })
  }

  async fetchPosts() {
    const posts = await apiClient.getPosts()
    let questions = posts.data.results.questions
    const mixedEntries = posts.data.results.posts.concat(questions)

    for (const post of posts.data.questions.data) {
      post["isQuestion"] = true
    }
    PostsModel.setPosts(posts.data.posts.data)
    PostsModel.setQuestions(questions)
    PostsModel.setMixedContent(mixedEntries)

    this.setState({ refreshing: false })
    this._setPostsToMobx()
  }

  _setPostsToMobx() {
    if (this.props.navigation.state.routeName === "postsScreen") {
      this.setState({ entry: PostsModel.getPosts })
    } else if (this.props.navigation.state.routeName === "questionsScreen") {
      this.setState({ entry: PostsModel.getQuestions })
    } else {
      this.setState({ entry: PostsModel.getMixedContent })
    }
  }

  componentDidMount() {
    if (PostsModel.getMixedContent.length > 0) {
      this._setPostsToMobx()
      this.setState({
        refreshing: false,
      })
      return
    }

    this.fetchPosts().then(() => {
      this._setPostsToMobx()
    })
  }

  render() {
    return (
      <ScrollView
        style={styles.mainContainer}
        refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh} />
        }
      >
        {Object.keys(this.state.entry).length > 0 &&
          this.state.entry.map(post => (
            <PostCard
              title={post.title}
              body={post.short_content}
              postImage={post.image}
              category={post.category_data.name}
              date={new Date(post.created_at)}
              onPress={() => this._onPress(post)}
              key={post.slug}
              username={post.user_data.username}
              userImage={post.user_details_data.profile_picture}
              userbadge="Adamin Dibi"
              userGender={post.user_details_data.gender}
            />
          ))}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  iconSize: 23,
  headerLogo: {
    maxHeight: 35,
    maxWidth: 200,
    marginTop: 5,
    marginLeft: 10,
  },
  headerView: {
    width: "100%",
    display: "flex",
    alignItems: "stretch",
    flexDirection: "row",
  },
  headerText: {
    marginLeft: "auto",
    padding: 10,
  },
  profilePicture: {
    width: 25,
    height: 25,
    borderRadius: 20,
    margin: 10,
  },
})
