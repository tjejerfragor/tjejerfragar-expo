import { Instance, SnapshotOut, types, applySnapshot } from "mobx-state-tree"
import { Result as FollowerType } from "@app/services/api/api.types"
import { toJS } from "mobx"

const Model = {
  id: types.maybeNull(types.identifierNumber),
  actor_details: types.model({
    id: types.maybeNull(types.number),
    about_me: types.maybeNull(types.string),
    relationship: types.maybeNull(types.string),
    career: types.maybeNull(types.string),
    city: types.maybeNull(types.string),
    profile_picture: types.maybeNull(types.string),
    main_picture: types.maybeNull(types.string),
    gender: types.maybeNull(types.string),
    user_level: types.maybeNull(types.number),
    sponsored: types.maybeNull(types.boolean),
    hidden_profile: types.maybeNull(types.boolean),
    user: types.maybeNull(types.number),
    user_data: types.model({
      id: types.maybeNull(types.number),
      username: types.maybeNull(types.string),
      first_name: types.maybeNull(types.string),
      last_name: types.maybeNull(types.string),
      email: types.maybeNull(types.string),
    }),
  }),
  user_details: types.model({
    id: types.maybeNull(types.identifierNumber),
    about_me: types.maybeNull(types.string),
    relationship: types.maybeNull(types.string),
    career: types.maybeNull(types.string),
    city: types.maybeNull(types.string),
    profile_picture: types.maybeNull(types.string),
    main_picture: types.maybeNull(types.string),
    gender: types.maybeNull(types.string),
    user_level: types.maybeNull(types.number),
    sponsored: types.maybeNull(types.boolean),
    hidden_profile: types.maybeNull(types.boolean),
    user: types.maybeNull(types.number),
    user_data: types.model({
      id: types.maybeNull(types.identifierNumber),
      username: types.maybeNull(types.string),
      first_name: types.maybeNull(types.string),
      last_name: types.maybeNull(types.string),
      email: types.maybeNull(types.string),
    }),
  }),
  created_at: types.maybeNull(types.string),
  updated_at: types.maybeNull(types.string),
  user: types.maybeNull(types.number),
  actor: types.maybeNull(types.number),
}

export const FollowModel = types
  .model("Follow", {
    followers: types.array(types.model(Model)),
    following: types.array(types.model(Model)),
  })
  .props({})
  .views(self => ({
    get getFollowers() {
      return self.followers.toJSON()
    },
    get getFollowings() {
      return self.following.toJSON()
    },
  }))
  .actions(self => ({
    setFollowers(data: Array<object>) {
      applySnapshot(self.followers, data)
    },
    setFollowings(data: Array<object>) {
      applySnapshot(self.following, data)
    },
  }))
  .create()

type FollowType = Instance<typeof FollowModel>
export interface Follow extends FollowType {}
type FollowSnapshotType = SnapshotOut<typeof FollowModel>
export interface FollowSnapshot extends FollowSnapshotType {}
