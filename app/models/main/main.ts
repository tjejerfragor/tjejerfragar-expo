import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { saveString } from "@app/utils/storage"

/**
 * States for application it self..
 */
export const MainModel = types
  .model("Main", {
    theme: types.maybeNull(types.string),
  })
  .props({})
  .views(self => ({
    get getCurrentTheme() {
      return self.theme
    },
  }))
  .actions(self => ({
    toggleTheme() {
      if (self.theme === "light") {
        self.theme = "dark"
      } else {
        self.theme = "light"
      }

      saveString("appTheme", self.theme)
      return self.getCurrentTheme
    },
    setTheme(theme: "light" | "dark" | string) {
      self.theme = theme
    },
  }))
  .create()

type MainType = Instance<typeof MainModel>
export interface Main extends MainType {}
type MainSnapshotType = SnapshotOut<typeof MainModel>
export interface MainSnapshot extends MainSnapshotType {}
