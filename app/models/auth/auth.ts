import { Instance, SnapshotOut, types, applySnapshot, getSnapshot } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */

const UserBasicData = types.model("UserBasic", {
  id: types.identifierNumber,
  username: types.maybeNull(types.string),
  first_name: types.maybeNull(types.string),
  last_name: types.maybeNull(types.string),
  email: types.maybeNull(types.string),
})

const userData = types.model("User", {
  id: types.identifierNumber,
  username: types.maybeNull(types.string),
  first_name: types.maybeNull(types.string),
  last_name: types.maybeNull(types.string),
  email: types.maybeNull(types.string),
  about_me: types.maybeNull(types.string),
  relationship: types.maybeNull(types.string),
  career: types.maybeNull(types.string),
  city: types.maybeNull(types.string),
  profile_picture: types.maybeNull(types.string),
  main_picture: types.maybeNull(types.string),
  gender: types.maybeNull(types.string),
  user_level: types.maybeNull(types.number),
  sponsored: types.maybeNull(types.boolean),
  hidden_profile: types.maybeNull(types.boolean),
  user: types.maybeNull(types.number),
  user_data: types.maybeNull(UserBasicData),
  token: types.maybeNull(types.string),
})

export const AuthModel = types
  .model("Auth", {
    token: types.maybeNull(types.string),
    userData: types.maybeNull(userData),
  })
  .props({})
  .views(self => ({
    get getToken() {
      return self.token
    },
    get getUserGender() {
      return self.userData.gender
    },
    get getUserName() {
      return self.userData.user_data.username
    },
  }))
  .actions(self => ({
    setToken(token: string) {
      self.token = token
    },
    setUserData(data) {
      self.userData = data
    },
  }))
  .create()

type AuthType = Instance<typeof AuthModel>
export interface Auth extends AuthType {}
type AuthSnapshotType = SnapshotOut<typeof AuthModel>
export interface AuthSnapshot extends AuthSnapshotType {}
