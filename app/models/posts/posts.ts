import { Instance, SnapshotOut, types, applySnapshot } from "mobx-state-tree"

"use strict"

const PostModel = {
  posts: types.model({
    id: types.identifierNumber,
    title: types.maybeNull(types.maybeNull(types.string)),
    content: types.maybeNull(types.string),
    slug: types.maybeNull(types.string),
    image: types.maybeNull(types.string),
    disable_comments: types.maybeNull(types.boolean),
    disable_man: types.maybeNull(types.boolean),
    disable_woman: types.maybeNull(types.boolean),
    disable_anonymous: types.maybeNull(types.boolean),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    user: types.maybeNull(types.number),
    category: types.maybeNull(types.number),
    user_details: types.maybeNull(types.number),
    user_data: types.model({
      id: types.maybeNull(types.number),
      username: types.maybeNull(types.string),
      first_name: types.maybeNull(types.string),
      last_name: types.maybeNull(types.string),
      email: types.maybeNull(types.string),
    }),
    user_details_data: types.model({
      id: types.maybeNull(types.number),
      about_me: types.maybeNull(types.string),
      relationship: types.maybeNull(types.string),
      career: types.maybeNull(types.string),
      city: types.maybeNull(types.string),
      profile_picture: types.maybeNull(types.string),
      main_picture: types.maybeNull(types.string),
      gender: types.maybeNull(types.string),
      user_level: types.maybeNull(types.number),
      sponsored: types.maybeNull(types.boolean),
      hidden_profile: types.maybeNull(types.boolean),
      user: types.maybeNull(types.number),
      user_data: types.model({
        id: types.maybeNull(types.number),
        username: types.maybeNull(types.string),
        first_name: types.maybeNull(types.string),
        last_name: types.maybeNull(types.string),
        email: types.maybeNull(types.string),
      }),
    }),
    short_content: types.maybeNull(types.string),
    category_data: types.model({
      id: types.maybeNull(types.number),
      name: types.maybeNull(types.string),
      description: types.maybeNull(types.string),
      slug: types.maybeNull(types.string),
      image: types.maybeNull(types.string),
    }),
    editor_picked: types.maybeNull(types.boolean),
  }),
}

const MixedContentModel = {
  posts: types.model({
    id: types.identifierNumber,
    title: types.maybeNull(types.maybeNull(types.string)),
    content: types.maybeNull(types.string),
    slug: types.maybeNull(types.string),
    image: types.maybeNull(types.string),
    disable_comments: types.maybeNull(types.boolean),
    disable_man: types.maybeNull(types.boolean),
    disable_woman: types.maybeNull(types.boolean),
    disable_anonymous: types.maybeNull(types.boolean),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    user: types.maybeNull(types.number),
    category: types.maybeNull(types.number),
    user_details: types.maybeNull(types.number),
    user_data: types.model({
      id: types.maybeNull(types.number),
      username: types.maybeNull(types.string),
      first_name: types.maybeNull(types.string),
      last_name: types.maybeNull(types.string),
      email: types.maybeNull(types.string),
    }),
    user_details_data: types.model({
      id: types.maybeNull(types.number),
      about_me: types.maybeNull(types.string),
      relationship: types.maybeNull(types.string),
      career: types.maybeNull(types.string),
      city: types.maybeNull(types.string),
      profile_picture: types.maybeNull(types.string),
      main_picture: types.maybeNull(types.string),
      gender: types.maybeNull(types.string),
      user_level: types.maybeNull(types.number),
      sponsored: types.maybeNull(types.boolean),
      hidden_profile: types.maybeNull(types.boolean),
      user: types.maybeNull(types.number),
      user_data: types.model({
        id: types.maybeNull(types.number),
        username: types.maybeNull(types.string),
        first_name: types.maybeNull(types.string),
        last_name: types.maybeNull(types.string),
        email: types.maybeNull(types.string),
      }),
    }),
    short_content: types.maybeNull(types.string),
    category_data: types.model({
      id: types.maybeNull(types.number),
      name: types.maybeNull(types.string),
      description: types.maybeNull(types.string),
      slug: types.maybeNull(types.string),
      image: types.maybeNull(types.string),
    }),
    editor_picked: types.maybeNull(types.boolean),
  }),
  questions: types.model({
    id: types.identifierNumber,
    title: types.maybeNull(types.maybeNull(types.string)),
    content: types.maybeNull(types.string),
    slug: types.maybeNull(types.string),
    image: types.maybeNull(types.string),
    disable_comments: types.maybeNull(types.boolean),
    disable_man: types.maybeNull(types.boolean),
    disable_woman: types.maybeNull(types.boolean),
    disable_anonymous: types.maybeNull(types.boolean),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    user: types.maybeNull(types.number),
    category: types.maybeNull(types.number),
    user_details: types.maybeNull(types.number),
    user_data: types.model({
      id: types.maybeNull(types.number),
      username: types.maybeNull(types.string),
      first_name: types.maybeNull(types.string),
      last_name: types.maybeNull(types.string),
      email: types.maybeNull(types.string),
    }),
    user_details_data: types.model({
      id: types.maybeNull(types.number),
      about_me: types.maybeNull(types.string),
      relationship: types.maybeNull(types.string),
      career: types.maybeNull(types.string),
      city: types.maybeNull(types.string),
      profile_picture: types.maybeNull(types.string),
      main_picture: types.maybeNull(types.string),
      gender: types.maybeNull(types.string),
      user_level: types.maybeNull(types.number),
      sponsored: types.maybeNull(types.boolean),
      hidden_profile: types.maybeNull(types.boolean),
      user: types.maybeNull(types.number),
      user_data: types.model({
        id: types.maybeNull(types.number),
        username: types.maybeNull(types.string),
        first_name: types.maybeNull(types.string),
        last_name: types.maybeNull(types.string),
        email: types.maybeNull(types.string),
      }),
    }),
    short_content: types.maybeNull(types.string),
    category_data: types.model({
      id: types.maybeNull(types.number),
      name: types.maybeNull(types.string),
      description: types.maybeNull(types.string),
      slug: types.maybeNull(types.string),
      image: types.maybeNull(types.string),
    }),
    content_type: types.number,
    hidden_author: types.boolean,
  }),
}

const QuestionModel = {
  questions: types.model({
    id: types.identifierNumber,
    title: types.maybeNull(types.maybeNull(types.string)),
    content: types.maybeNull(types.string),
    slug: types.maybeNull(types.string),
    image: types.maybeNull(types.string),
    disable_comments: types.maybeNull(types.boolean),
    disable_man: types.maybeNull(types.boolean),
    disable_woman: types.maybeNull(types.boolean),
    disable_anonymous: types.maybeNull(types.boolean),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    user: types.maybeNull(types.number),
    category: types.maybeNull(types.number),
    user_details: types.maybeNull(types.number),
    user_data: types.model({
      id: types.maybeNull(types.number),
      username: types.maybeNull(types.string),
      first_name: types.maybeNull(types.string),
      last_name: types.maybeNull(types.string),
      email: types.maybeNull(types.string),
    }),
    user_details_data: types.model({
      id: types.maybeNull(types.number),
      about_me: types.maybeNull(types.string),
      relationship: types.maybeNull(types.string),
      career: types.maybeNull(types.string),
      city: types.maybeNull(types.string),
      profile_picture: types.maybeNull(types.string),
      main_picture: types.maybeNull(types.string),
      gender: types.maybeNull(types.string),
      user_level: types.maybeNull(types.number),
      sponsored: types.maybeNull(types.boolean),
      hidden_profile: types.maybeNull(types.boolean),
      user: types.maybeNull(types.number),
      user_data: types.model({
        id: types.maybeNull(types.number),
        username: types.maybeNull(types.string),
        first_name: types.maybeNull(types.string),
        last_name: types.maybeNull(types.string),
        email: types.maybeNull(types.string),
      }),
    }),
    short_content: types.maybeNull(types.string),
    category_data: types.model({
      id: types.maybeNull(types.number),
      name: types.maybeNull(types.string),
      description: types.maybeNull(types.string),
      slug: types.maybeNull(types.string),
      image: types.maybeNull(types.string),
    }),
    content_type: types.number,
    hidden_author: types.boolean,
  }),
}

export const PostsModel = types
  .model("Posts", {
    posts: types.array(PostModel.posts),
    questions: types.array(QuestionModel.questions),
    mixedContent: types.array(MixedContentModel.posts),
  })
  .props({})
  .views(self => ({
    get getPosts() {
      return self.posts.toJSON()
    },
    get getQuestions() {
      return self.questions.toJSON()
    },
    get getMixedContent() {
      return self.mixedContent.toJSON()
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    setPosts(data) {
      applySnapshot(self.posts, data)
    },
    setQuestions(data) {
      applySnapshot(self.questions, data)
    },
    setMixedContent(data) {
      applySnapshot(self.mixedContent, data)
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .create()

type PostsType = Instance<typeof PostsModel>
export interface Posts extends PostsType {}
type PostsSnapshotType = SnapshotOut<typeof PostsModel>
export interface PostsSnapshot extends PostsSnapshotType {}
