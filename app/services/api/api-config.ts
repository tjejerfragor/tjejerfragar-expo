import * as env from "../../environment-variables"

/**
 * The options used to configure the API.
 */
export interface ApiConfig {
  /**
   * The URL of the api.
   */
  url: string

  /**
   * Milliseconds before we timeout the request.
   */
  timeout: number
}

export interface ApiHeaders {
  Accept: string
  Authorization?: string
}

/**
 * The default configuration for the app.
 */
export const DEFAULT_API_CONFIG: ApiConfig = {
  url: env.API || "https://jsonplaceholder.typicode.com",
  timeout: 10000,
}

export const DEFAULT_API_HEADERS: ApiHeaders = {
  Accept: "application/json",
}
