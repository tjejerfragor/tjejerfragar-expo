import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG, DEFAULT_API_HEADERS, ApiHeaders } from "./api-config"
import * as Types from "./api.types"
import { AuthModel } from "@app/models/auth"
import { backendApi } from "../../../app.config.json"
/**
 * Manages all requests to the API.
 */

interface test {
  token: string
  postId?: number
  questionId?: number
  commentId?: number
}
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  headers: ApiHeaders

  apiUrl = backendApi

  userToken: string = AuthModel.getToken || ""

  postLikeApiUrl = (isQuestion, entryId) =>
    isQuestion ? `/likes/?post=&question=${entryId}` : `/likes/?question=&post=${entryId}`

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG, headers: ApiHeaders = DEFAULT_API_HEADERS) {
    this.config = config
    this.config.url = this.apiUrl
    this.headers = headers
    if (this.userToken) {
      this.headers.Authorization = `Token ${this.userToken}`
    }
    this.setup()
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      // ...this.headers
    })
  }

  /**
   * Gets a list of users.
   */
  async getUsers(): Promise<Types.GetUsersResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const convertUser = raw => {
      return { id: raw.id, name: raw.name }
    }

    // transform the data into the format we are expecting
    try {
      const rawUsers = response.data
      const resultUsers: Types.User[] = rawUsers.map(convertUser)
      return { kind: "ok", users: resultUsers }
    } catch {
      return { kind: "bad-data" }
    }
  }

  async login(email: string, password: string) {
    const data = { email: email, password: password }

    const response: ApiResponse<Types.Login> = await this.apisauce.post("login/", data)

    return response
  }

  async getUserInformation(token: string) {
    this.apisauce.setHeader("Authorization", `Token ${token}`)

    const request: ApiResponse<Types.User> = await this.apisauce.get("user/")

    return request
  }

  async getPosts() {
    const request: ApiResponse<Types._AllEntries> = await this.apisauce.get("all")

    return request
  }

  async likeEntry(entryId: number, data: object) {
    const request: ApiResponse<Types.likeEntry> = await this.apisauce.post(
      `/likes/${entryId}/`,
      data,
    )

    return request
  }

  async unLikeEntry(entryId: number, data: object, token: string) {
    this.apisauce.setHeader("Authorization", `Token ${token}`)
    console.log(data)
    const request: ApiResponse<Types.likeEntry> = await this.apisauce.delete(
      `/likes/${entryId}/`,
      data,
    )

    return request
  }

  async getEntryCommentCount(slug: string) {
    const request: ApiResponse<Types.Comment> = await this.apisauce.get(`comments/?search=${slug}`)

    return request
  }

  async getPostLikeCount(isQuestion: boolean, entryId: number) {
    const request: ApiResponse<Types.PostLike> = await this.apisauce.get(
      this.postLikeApiUrl(isQuestion, entryId),
    )

    return request
  }

  async getFollowers(userId: number, token: string) {
    this.apisauce.setHeader("Authorization", `Token ${token}`)

    const request: ApiResponse<Types.Follow> = await this.apisauce.get(
      `/followers/?actor=${userId}&user=`,
    )

    return request
  }

  async getFollowing(userId: number, token: string) {
    this.apisauce.setHeader("Authorization", `Token ${token}`)

    const request: ApiResponse<Types.Follow> = await this.apisauce.get(
      `/followers/?user=${userId}&actor=`,
    )
    return request
  }

  async getUserDetails(userName: string) {
    const request: ApiResponse<Types.PaginationUser> = await this.apisauce.get(
      `/userdetail/?user__username=${userName}`,
    )

    return request
  }

  async getUserQuestions(userId: number) {
    const request = await this.apisauce.get(`/questions/?category=&category__name=&user=${userId}`)

    return request
  }

  async followUser(actorId: number, token?: string) {
    const data = { actor: actorId }
    this.apisauce.setHeader("Authorization", `Token ${token}`)

    const request: ApiResponse<Types.FollowingUserResult> = await this.apisauce.post(
      `/followers/`,
      data,
    )

    return request
  }

  async isAlreadyLiked(context: test) {
    const { postId, commentId, questionId, token } = context
    this.apisauce.setHeader("Authorization", `Token ${token}`)

    const endpointUri = `/liked/${postId || questionId || commentId}`
    const request: ApiResponse<Types.IsAlreadyLiked> = await this.apisauce.get(endpointUri)

    return request
  }
}
