import { GeneralApiProblem } from "./api-problem"

export interface Login {
  token: string
}
export interface UserData {
  id: number
  username: string
  first_name: string
  last_name: string
  email: string
}

export interface _User extends Object {
  id: number
  username: string
  first_name: string
  last_name: string
  email: string
  about_me: string
  relationship?: any
  career?: any
  city?: any
  profile_picture: string
  main_picture?: any
  gender: string
  user_level: number
  sponsored: boolean
  hidden_profile: boolean
  user: number
  user_data: UserData
  [Symbol.iterator]()
}

export interface User {
  user: _User
}

export interface PaginationUser {
  count: number
  next?: any
  previous?: any
  results: _User
}

export interface CategoryData {
  id: number
  name: string
  description: string
  slug: string
  image: string
}

export interface _Post extends Array<object> {
  id: number
  title: string
  content: string
  slug: string
  image: string
  disable_comments: boolean
  disable_man: boolean
  disable_woman: boolean
  disable_anonymous: boolean
  created_at: Date
  updated_at: Date
  user: number
  category: number
  user_details: number
  user_data: UserData
  user_details_data: _User
  short_content: string
  category_data: CategoryData
  editor_picked: boolean
}

export interface _Question extends Array<object> {
  id: number
  title: string
  content: string
  slug: string
  image: string
  disable_comments: boolean
  disable_man: boolean
  disable_woman: boolean
  disable_anonymous: boolean
  created_at: Date
  updated_at: Date
  user: number
  category: number
  user_details: number
  user_data: UserData
  user_details_data: _User
  short_content: string
  category_data: CategoryData
  content_type: number
  hidden_author: boolean
}

export interface _AllEntriesResults extends Array<object> {
  posts: _Post
  questions: _Question
}

export interface _AllEntries {
  count: number
  next?: any
  previous?: any
  results: _AllEntriesResults
}

export interface likeEntry {
  status: boolean
}

export interface _Comment {
  id: number
  post: number
  user: number
  question?: any
  parent?: any
  hidden_author: boolean
  content: string
  created_at: Date
  user_details: number
  user_data: UserData
  user_details_data: _User
  post_data: _Post
  question_data?: any
  [Symbol.iterator]
}

export interface Comment {
  count: number
  next?: any
  previous?: any
  results: _Comment
}

export interface _PostLike {
  id: number
  girl_like_count: number
  boy_like_count: number
  girl_disslike_count: number
  boy_disslike_count: number
  question?: any
  post: number
  comment?: any
}

export interface PostLike {
  count: number
  next?: any
  previous?: any
  results: _PostLike
}

export interface ActorDetails {
  id: number
  about_me: string
  relationship?: any
  career?: any
  city?: any
  profile_picture: string
  main_picture: string
  gender: string
  user_level: number
  sponsored: boolean
  hidden_profile: boolean
  user: number
  user_data: UserData
}

export interface Result {
  id: number
  actor_details: ActorDetails
  user_details: _User
  created_at: Date
  updated_at: Date
  user: number
  actor: number
}
export interface Follow {
  count: number
  next?: any
  previous?: any
  results: Result[]
}

export interface FollowingUserResult {
  status: boolean
  status_code: string
}

export interface IsAlreadyLiked {
  id: number
  liked: boolean
  dissliked: boolean
  user: number
  question: number | null
  post: number | null
  comment: number | null
}

export type GetUsersResult = { kind: "ok"; users: User[] } | GeneralApiProblem
export type GetUserResult = { kind: "ok"; user: User } | GeneralApiProblem
